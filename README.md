INTRODUCTION
------------

The Domain Route Access module provides a new configuration entity to manage
access to existing routes by domain.
For each Domain Route Access configuration entity, you could:
- Select the route for which you want to check access by domains
- Select the allowed domains on which the route should be accessible
- Enable/Disable the access check

REQUIREMENTS
------------

This module requires Domain (domain) contrib module.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

To add new Domain Route Access config entity:

1. Go to Domain Route Access config entity collection page:

`/admin/config/domain/route-access`

2. Click `Add new Domain Route Access` action link
3. Fill in the new Domain Route Access config entity form and click Save
4. Clear Drupal cache

FOR DEVELOPERS
--------------

Developers could manage access of custom route using existing
`_domain` route requirement:

    my_module.custom_route:
      path: '/foo/bar'
      defaults:
        _controller: 'Drupal\my_module\Controller\BarsController'
      requirements:
        _permission: 'view bars content'
        _domain: 'domain1_id+domain2_id'

MAINTAINERS
-----------

- [Brahim KHOUY](https://www.drupal.org/u/bkhouy)
