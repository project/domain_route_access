<?php

namespace Drupal\domain_route_access\Entity;

/**
 * Domain Route Access interface.
 */
interface DomainRouteAccessInterface {

  /**
   * Get related route name.
   *
   * @return string
   */
  public function getRouteName(): string;

  /**
   * Returns allowed domains.
   *
   * @return array|null
   */
  public function getDomains(): ?array;

  /**
   * Check whether the domain route access config entity is enabled.
   *
   * @return bool
   */
  public function isActive(): bool;

}
