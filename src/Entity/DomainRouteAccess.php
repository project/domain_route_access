<?php

namespace Drupal\domain_route_access\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Domain route access entity.
 *
 * @ConfigEntityType(
 *   id = "domain_route_access",
 *   label = @Translation("Domain route access"),
 *   handlers = {
 *     "list_builder" = "Drupal\domain_route_access\Controller\DomainRouteAccessListBuilder",
 *     "form" = {
 *       "add" = "Drupal\domain_route_access\Form\DomainRouteAccessForm",
 *       "edit" = "Drupal\domain_route_access\Form\DomainRouteAccessForm",
 *       "delete" = "Drupal\domain_route_access\Form\DomainRouteAccessDeleteForm",
 *     },
 *   },
 *   config_prefix = "domain_route_access",
 *   admin_permission = "administer domain_route_access",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "route_name" = "route_name",
 *     "domains" = "domains",
 *   },
 *   links = {
 *     "collection" = "/admin/config/domain/route-access",
 *     "edit-form" = "/admin/config/domain/route-access/{domain_route_access}/edit",
 *     "delete-form" = "/admin/config/domain/route-access/{domain_route_access}/delete",
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "uuid" = "uuid",
 *     "route_name" = "route_name",
 *     "domains" = "domains",
 *   }
 * )
 */
class DomainRouteAccess extends ConfigEntityBase implements DomainRouteAccessInterface {

  /**
   * The domain route access ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The domain route access label.
   *
   * @var string
   */
  protected $label;

  /**
   * The domain route access route name.
   *
   * @var string
   */
  public $route_name;

  /**
   * The allowed domains.
   *
   * @var array
   */
  public $domains;

  /**
   * {@inheritDoc}
   */
  public function getRouteName(): string {
    return $this->route_name ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function getDomains(): ?array {
    return $this->domains ? array_filter($this->domains) : [];
  }

  /**
   * {@inheritDoc}
   */
  public function isActive(): bool {
    return $this->status();
  }

}
