<?php

namespace Drupal\domain_route_access\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Domain route access add and edit forms.
 */
class DomainRouteAccessForm extends EntityForm {

  /**
   * Route provider service.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * Constructs DomainRouteAccess form object.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RouteProviderInterface $route_provider,) {
    $this->entityTypeManager = $entityTypeManager;
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('router.route_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $domain_route_access = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $domain_route_access->label(),
      '#description' => $this->t("Label for the domain route access."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $domain_route_access->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$domain_route_access->isNew(),
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $domain_route_access->status(),
      '#description' => $this->t('If checked then the domain access check process will be applied on selected route'),
    ];

    $routes = [];
    foreach ($this->routeProvider->getAllRoutes() as $name => $route) {
      $path = $route->getPath();
      $routes[$name] = _filter_html_escape("$name [ $path ]");
    }

    $selected_route_element = '<div id="selected-route-name">';
    $selected_route_element .= !empty($domain_route_access->getRouteName()) ? 'Selected route: ' . $routes[$domain_route_access->getRouteName()] : '';
    $selected_route_element .= '</div>';

    $form['route_name'] = [
      '#prefix' => "<div class='route-access-scrollable-area'>$selected_route_element",
      '#suffix' => '<p class="fieldset__description">' . $this->t('Select the route you wish apply domain access on.') . '</p></div>',
      '#type' => 'radios',
      '#title' => $this->t('Route'),
      '#options' => $routes,
      '#default_value' => $domain_route_access->getRouteName(),
      '#required' => TRUE,
      '#description' => $this->t('Select the route you wish apply domain access on.'),
    ];

    $domains_records = $this->entityTypeManager->getStorage('domain')->loadMultiple();
    $domains = [];
    foreach ($domains_records as $domains_record) {
      $domains[$domains_record->id()] = $domains_record->label();
    }
    $form['domains'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed domains'),
      '#options' => $domains,
      '#default_value' => $domain_route_access->getDomains(),
      '#required' => TRUE,
      '#description' => $this->t('The route will be accessible on selected domains'),
    ];

    $form['#attached']['library'][] = 'domain_route_access/style';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $domain_route_access = $this->entity;
    $status = $domain_route_access->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label Domain Route Access has been created.', [
        '%label' => $domain_route_access->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Domain Route Access has been updated.', [
        '%label' => $domain_route_access->label(),
      ]));
    }

    // Redirect to Domain Route Access collection page.
    $form_state->setRedirect('entity.domain_route_access.collection');
    
    return $status;
  }

  /**
   * Helper function to check whether a domain route access configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('domain_route_access')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
