<?php

namespace Drupal\domain_route_access\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Domain Route Access subscriber class.
 *
 * @package Drupal\domain_route_access\Routing
 */
class DomainRouteAccessRouteSubscriber extends RouteSubscriberBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Domain Route Access Route Subscriber Constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }


  /**
   * Alters existing routes for enabled Domain Route Access entries.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection for adding routes.
   */
  protected function alterRoutes(RouteCollection $collection) {
    $domain_route_access_ids = $this->entityTypeManager->getStorage('domain_route_access')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', 1)
      ->execute();
    if (empty($domain_route_access_ids)) {
      // No enabled Domain Route Access case, do nothing.
      return;
    }

    // Get enbaled Domain Route Access entities.
    $domain_route_access = $this->entityTypeManager->getStorage('domain_route_access')
      ->loadMultiple($domain_route_access_ids);
    foreach ($domain_route_access as $route_access_item) {
      $route_name = $route_access_item->getRouteName();
      $allowed_domains = $route_access_item->getDomains();
      if (empty($allowed_domains)) {
        // Skip processing current Domain Route Access item.
        continue;
      }

      // Alter the related Domain Route Access route.
      if ($route = $collection->get($route_name)) {
        // Add Domain Access check for current route.
        $route->setRequirement('_domain', implode('+', $allowed_domains));
      }
    }
  }

}
