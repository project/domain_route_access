<?php

namespace Drupal\domain_route_access\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Domain Route Access entity list builder.
 */
class DomainRouteAccessListBuilder extends ConfigEntityListBuilder {

  /**
   * Route provider service.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->routeProvider = $container->get('router.route_provider');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function buildHeader() {
    $header = [
      'status' => $this->t('Active'),
      'label' => $this->t('Label'),
      'route_name' => $this->t('Route name'),
      'domains' => $this->t('Allowed domains'),
      'id' => $this->t('ID'),
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['status'] = $entity->isActive() ? '🟢' : '🔴';
    $row['label'] = $entity->label();
    $row['route_name'] = $entity->getRouteName();
    $row['domains'] = implode(', ', $entity->getDomains());
    $row['id'] = $entity->id();
    return $row + parent::buildRow($entity);
  }

  public function render() {
    // Build introduction for clearing cache instructions.
    $url = Url::fromRoute('domain_route_access.clear_cache')->toString(TRUE)->getGeneratedUrl();
    $html = "<div class='color-checked form-item__description'>";
    $html .= "🚨 Rebuild cache is required after Domain Route Access changes (Add, Update or Delete operations). ";
    $html .= "<a href='$url'>Clear Cache Now</a><br>";
    $html .= "We can flush the cache programmatically after each Domain Route Access change, but for performance reasons we recommend clearing the cache manually after you have completed all Domain Route Access changes.";
    $introduction = [
      '#markup' => $html
    ];

    // Get the parend list builder render.
    $build = parent::render();
    $enabled_rows = array_filter($build['table']['#rows'], function ($row) { return $row['status']; });
    // Enabled domain route access entities route names.
    $enabled_route_names = [];
    if (!empty($enabled_rows)) {
      $enabled_route_names = array_map(function ($row) { return $row['route_name']; }, $enabled_rows);
    }

    // The approach is to loop through all existing routes and find those with;
    // Requirement _domain, we have to not only list Domain Route Access;
    // Entities but also other routes that have _domain as requirement;
    foreach ($this->routeProvider->getAllRoutes() as $name => $route) {
      if (!$route->hasRequirement('_domain')) {
        continue;
      }
      if (in_array($name, $enabled_route_names)) {
        continue;
      }
      $route_domains = array_map('trim', explode('+', $route->getRequirement('_domain')));
      $build['table']['#rows'][] = [
        'status' => '🟢',
        'label' => 'Applied programmatically',
        'route_name' => $name,
        'domains' => implode(', ', $route_domains),
        'id' => '-',
        'operations' => '-',
      ];
    }
    return [
      $introduction,
      $build,
    ];
  }

}
