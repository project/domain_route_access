<?php

namespace Drupal\domain_route_access\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DomainRouteAccessController extends ControllerBase {

  /**
   * Clear the rendered cache.
   */
  public function cacheRender() {
    $this->messenger()->addMessage($this->t('Cache has been cleared successfully.'));
    drupal_flush_all_caches();
    return new RedirectResponse(Url::fromRoute('entity.domain_route_access.collection')->toString(TRUE)->getGeneratedUrl());
  }

}
