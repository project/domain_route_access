(function ($, Drupal) {
  Drupal.behaviors.domainRouteAccess = {
    attach: function (context) {
      $('input[name="route_name"]').on('change', function () {
        var checkedLabel = $('.route-access-scrollable-area input[name="route_name"]:checked').siblings('label')
        if (checkedLabel.length > 0) {
          $('#selected-route-name').text('Selected route: ' + checkedLabel.text().trim())
        }
        else {
          $('#selected-route-name').empty()
        }
      })
    }
  }
})(jQuery, Drupal)
